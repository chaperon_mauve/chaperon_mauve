# Personnages utilisés dans le jeu.
define c = Character(_('Chap\''), color="#E0B0FF", image="chaperon")
define l = Character(_('Loup'), color="#FF0000", image="loup")
define m = Character(_('Mère-Grand'), color="#CCCCCC")
define pa = Character(_('Paricil Amonai'))
image bg tale = "tale.png"
image bg kitchen = "kitchen.png"
image bg street = "street.png"
image bg phone = "phone.png"
image bg hallway = "hallway.png"
image bg bedroom mamie happy = "bedroom-mamie-happy.png"
image bg bedroom mamie worried = "bedroom-mamie-worried.png"
image bg bedroom loup = "bedroom-loup.png"
image chaperon happy = "chaperon-happy.png"
image chaperon angry = "chaperon-angry.png"
image chaperon horrified = "chaperon-horrified.png"
image loup = "loup.png"
image loup grimace = "loup-grimace.png"
image bg ending grounded = "ending-grounded.png"
image bg ending victim   = "ending-victim.png"
image bg ending myprisoner   = "ending-myprisoner.png"
# Add outline to ending titles (avoid white text on white background)
image text_ending = renpy.ParameterizedText(outlines=[(2, "#000", 0, 0)], yalign=0.5)

# Le jeu commence ici
label start:
    # L33t cloudy/coton markee intro effect
    define fadewhite = Fade(0.0, 0.0, 2.0, color="#fff")
    image bg tale_quad:
        im.Composite((1280*2, 720*2),
                      (0, 0), "tale.jpg",
                      (1280, 0), "tale.jpg",
                      (0, 720), "tale.jpg",
                      (1280, 720), "tale.jpg")
        anchor (0,0)
        pos (0, 0)
        linear 5.0 pos (-1280, -720)
        repeat
    scene bg tale_quad with fadewhite

    #play music "intro.ogg"
    "Il était une fois..."
    "dans une contrée lointaine..."

    scene bg kitchen
    stop music
    play sound "record_scratch.ogg"
    show chaperon angry with vpunch
    c "M'maaan !"
    play music "street.ogg"
    c "J'ai plus l'âge que tu me racontes des histoires !"

    "Puisque c'est comme ça, jeune fille, rends-toi utile et va porter
     ces friandises à ta grand-mère."
    "Elle est clouée au lit depuis deux jours."


    menu:
        "Obéis":
            jump oui
        "Décline la proposition":
            jump non

label non:
    c "'tain, j'en ai marre..."
    c "Ça tombe toujours sur moi, tu me m'saoûles !"
    "Petit chaperon, je ne veux pas entendre ce langage !"
    c "J'suis plus \"petit\" et d'abord j'irai si j'ai envie !"
    "Ah oui ?"
    "Eh bien cet après-midi tu restes dans ta chambre et tu finis
     tes devoirs, je t'interdis de sortir."
    "Quant à ta soirée de samedi, on en reparlera avec ton père..."
    c horrified "Heeeiiin ?!"

    scene bg ending grounded
    with dissolve
    show text_ending _("MAUVAISE FIN - PUNIE")
    c "C'est pas juste !"

    return

label oui:
    c "Pfff... d'accord."
    "Prends cette galette de riz bio et ce petit pot de beurre."
    c "Tu crois pas qu'elle a assez grossi comme ça ?"
    c "Elle rentre à peine dans sa chaise."
    "Je ne veux pas t'entendre dire du mal de ta grand-mère."
    "Même si c'est vrai."
    c "Bon ben j'y go, à plus."


    scene bg street
    show chaperon angry
    with dissolve
    c "Au moins j'ai mon smartphone pour me tenir compagnie pendant le trajet..."
    c "{i}RT si t'en as marre de te taper les corvées #mere #relou{/i}"
    c "{i}Statut : je me tape tout le boulevard Wilson à pied jusqu'au n°173, pfff !{/i}"
    show loup at right with moveinright
    l "Hello !"

    # Loup se montre très séducteur avec Chap', qui ajoute "Loup3769"
    # sur SocialBook, comme elle le fait souvent pour les gens qu'elle
    # croise pour la première fois... alors qu'elle vient d'y décrire
    # précisément où habite sa grand-mère.

    show chaperon happy
    c "Euh... Bonjour ?"
    l "À te voir ainsi froncer les sourcils, je parie qu'on t'a encore mise à contribution chez toi."
    c "Ben euh... ouais... Enfin non, ma mère m'a fait pitié alors j'y vais mais juste pour cette fois."
    c "Faut pas déconner, hein ?"
    l "Hmmm, les parents ont oublié ce que c'est que d'être jeunes et de vouloir profiter,"
    l "de vivre vraiment quoi !"
    c "Ouais..."
    l "Ce qu'il faut c'est avoir les tripes d'imposer ses choix, et de préparer les vieux à prendre son indépendance."
    l "Je sûr que tu en es capable. T'es jeune, vive, mignonne..."
    c "Oh, non, v-tu... c'est exagéré !"
    l "Mais quand on est de jeunes adultes comme toi et moi, on sait que c'est maintenant que ça se passe, après ce sera trop tard, t'es pas d'accord ?"
    c "Si, mais..."
    l grimace "Voilà, et mes parents ils ont vite dû comprendre que je n'étais plus leur \"petit chéri\", ewww !"
    c "Hihi, c'est exactement ce que j'ai dit à ma mère !"
    show loup
    l "Tu vois je t'ai dit que tu avais ça en toi."
    l "Je ne veux plus voir ce joli front plissé par les soucis, OK ?"
    c "Bah, pour cet après-midi c'est mort, mais ouais la prochaine fois ça sera non, hé."
    l "Du coup tu vas où exactement cet après-midi ?"
    c "Euh... je vais voir de la famille... plus loin."
    l "Et c'est où ça \"plus loin\" ?"
    c "Ben c'est... par là-bas mais... euh faut peut-être que j'y aille d'ailleurs..."
    l "Ne me dis pas que tu es {i}pressée{/i} d'y aller ?"
    c "Non bien sûr... tiens, je vais faire un petit crochet pour me prendre une glace !"
    l "Héhé... Tiens écoute, il a une soirée vendredi, on y va avec des potes et leurs copines."
    l "On a besoin de gens comme toi pour montrer à tous comment s'éclater."
    l "Ajoute-moi sur SocialBook, tu cherches \"Loup3769\", ok?, et tu me ping pour que je file les détails."
    c "OK, \"Loup3769\", à plus !"
    c "Bye."
    hide loup with moveoutright
    c "(Bon, et maintenant...)"


    menu:
        "Ajoute Loup sur SocialBook et traîne en chemin":
                jump dangerous
        "Dépêche-toi d'arriver chez Mamie":
                jump peaceful_afternoon
        
label peaceful_afternoon:
    c "Bon c'est toujours sympa de se faire draguer, mais j'ai pas que ça à faire moi."
    c "Quant à sa demande d'ami, il a fumé ou quoi ?  Y'a pas moyen, on se connaît pas."
    scene bg bedroom mamie happy
    with dissolve
    play music "mamie.ogg"
    c "Hello Mamie"
    m "Bonjour ma petite ! Ça me fait bien plaisir de t'avoir avec moi."
    c "..."
    c "Moi aussi Mamie !"
    m "Que dirais-tu d'un petit scrabble ? Je parie que je te bats encore cette fois-ci, hihi !"
    c "Dans tes rêves ! Tu vas voir..."
    
    scene black
    with dissolve
    show text_ending _("FIN TRANQUILLE - JEUX")
    m "Mot compte double !"

    return


label dangerous:
    c "Je parie qu'il a cru que j'étais plus agée, il a la classe quand même."
    c "Je vais l'ajouter comme ami, ça va faire criser mes copines !"

    scene bg phone
    with dissolve
    "Chap Petit souhaite vous inviter à rejoindre son groupe d’amis sur SocialBook."
    "Statut : {i}En plus je me tape tout le boulevard Wilson à pied jusqu'au n°173, pfff !{/i}"
    l "Petite sotte..."
    l "Hé bien on va aller y faire un tour."

    # Loup devance Chap' chez sa grand-mère dans le dessein d'abuser
    # de la jeune fille...
    scene bg hallway
    with dissolve
    play music "mamie.ogg"
    l "Voilà, \"Margaret Petit\", c'est le nom sur la première porte à droite."


    menu:
        "Aggresse Mamie":
                jump aggression
        "Manœuvre en finesse":
                jump derrick

label aggression:
    l "(La porte est carrément déverrouillée, trop facile !)"

    scene bg bedroom mamie worried
    with dissolve

    m "Qui est-ce ?  Que voulez-vous ?  Fichez le camp avant que j'appelle la police !"
    l "La ferme la vieille !  Tu vas faire ce que je dis et..."
    stop music
    play sound "spray1.ogg"
    l "AAAARRRRRRGHHHH !!!"
    play music "mamie-fast.ogg" noloop
    m "Monsieur, voici Suzie, ma fidèle petite bombe lacrymo.  Suzie, re-présente toi au monsieur."
    play sound "spray2.ogg"
    l "UUURRRRRRRRRGGGGGH !!!!"
    m "Et maintenant, comme promis, vous allez également faire connaissance avec les forces de l'ordre. Non mais !"
    
    scene black
    with dissolve
    show text_ending _("BONNE FIN (... enfin mauvaise fin pour Loup !) - CASE PRISON")
    "Agent" "Chère Madame, la police vous adresse ses félicitations pour votre sang froid.\nÇa mettra du plomb dans la cervelle de cette petite frappe !"

    return


label derrick:
    l "Toc toc !"
    m "Oui ? Qui est là ?"
    l "Chère madame PETIT, je suis représentant, et figurez-vous que j'ai sur moi l'INTÉGRALE,"
    l "je dis bien l'INTÉGRALE de DERRICK, à prix SACRIFIÉ, et tenez-vous bien,"
    l "j'inclus même la confrontation censurée DERRICK/NAVARRO !"
    m "Oohh... Vous savez parler aux femmes vous, hihi !"
    m "Tirez la chevillette, et la bobinette cherra."
    l "(Euh... ça veut dire que la porte était carrément déverrouillée ?!)"
    scene black
    with dissolve
    m "Alors montrez-moi ça."
    m "Mais que faites-vous ?  À l'... MMMHHH !"

    scene bg hallway
    with dissolve
    c "Toc toc, Mamie, ouh ouh !"
    l "Oui ? Qui est là ?"
    c "Bah c'est moi, Chap', enfin !"
    l "Tire la chevillette, et la bobinette cherra."
    c "Raaah, Mamie ! Essaie de parler un peu moins vieux, quoi, t'abuses."
    c "Puis je sais ouvrir une porte quand même."

    scene bg bedroom loup
    with dissolve
    c "Bah, qu'est-ce que tu t'es mis sur le visage Mamie ?"
    c "Tu t'es crue dans une série US ?  C'est pas des concombres qui vont t'enlever tes rides, hé !"
    l "Hrmm, ma petite, quand tu auras mon âge, on en reparlera."
    c "Hmmmm..."


    menu:
        "Ne contrarie pas Mamie":
            jump stay
        "Enfile tes lunettes (même si tu es moche avec)":
            jump glasses

label stay:
    # La fin du conte de Perrault finit mal (pas de chasseur), ce sera
    # la mauvaise fin.  D'ailleurs je n'aime pas le personnage du
    # chasseur qui ressuscite les morts, c'est un peu facile, ici on
    # jouera plutôt sur les choix du joueur.

    l "Mets-nous un petit peu de ta musique de jeunes."
    play music "club.ogg" fadein 4.0
    l "Plus fort, comme tu as l'habitude, hihi !"
    l "Maintenant approche mon enfant..."
    c "Oui... ?"
    c "Hé lâche-moi !"
    c "Mais qui êtes-vous ?"
    c "À l'aide ! Au secours ! Maman !"
    l "Laisse-toi faire..."

    scene bg ending victim
    with dissolve
    show text_ending _("MAUVAISE FIN - VICTIME")
    "Loup fait quelque chose de très désagréable à Chap'.\nQuand pour finir ses pattes se serrent autour de son cou,\nles ténèbres arrivent comme une délivrance."
    
    return

label glasses:

    c "(Mais c'est Loup ! 'tain y se FOUT DU MONDE quoi, ça ne ve pas se passer comme ça !)"
    c "Mamie je reviens j'avais dit à mon petit copain je le rappelais, je fais vite."
    l "(Petit copain, petit copain... Quand je me serais occupé de toi, je n'aurai plus rien à envier à celui-là...)"
    c "Oui, maison de retraite \"Parici Lamonai?\" ? C'est pour une urgence, je suis au 173 boulevard Wilson."
    c "Ça fait un moment qu'on suspecte quelque chose chez ma grand-mère, Mme Petit, mais là c'est confirmé..."
    c "Je viens de lui rendre visite et elle s'est carrément mis des fruits et légumes sur la tête !"
    pa "Très chère mademoiselle, nous arrivons immédiatement pour prendre en charge le cli... patient."
    c "Je savais que je pouvais compter sur vous !"
    play sound "siren.ogg"
    pa "C'est ici, attrapez-la !"
    l "Quoi ? Mais euh, je vous interdis lâchez-moi !"
    scene bg bedroom loup with vpunch
    pa "Soyez raisonnable madame."
    l "(Bon pas le choix, je dois me démasquer) Mais enfin, tout ceci était une plaisanterie, vous voyez bien que je suis un jeune loup !"
    pa "Mademoiselle, vous aviez raison, elle yoyote complètement, on va la tenir sous bonne garde."
    l "AU SECOURS !!!!"

    scene bg ending myprisoner
    with dissolve
    show text_ending _("BONNE FIN (pour Chap' ! - mauvaise fin pour Loup !) - MON PRISONNIER")
    pa "Chère demoiselle Chap', malgré ses tentatives d'évasion, votre grand-mère vit maintenant dans un environnement sûr pour elle. Vous avez fait le bon choix avec nous."

    return
