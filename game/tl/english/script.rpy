﻿# Translation: bosinpai
# Proofreading: Hazel-Bun of Cherubim Scribes

translate english strings:

    # game/script.rpy:2
    old "Chap'"
    new "Mo'"

    # game/script.rpy:3
    old "Loup"
    new "Wolf"

    # game/script.rpy:4
    old "Mère-Grand"
    new "Grandmother"

    # game/script.rpy:5
    old "Paricil Amonai"
    new "Handov Erzycash"

# game/script.rpy:37
translate english start_be560584:

    # "Il était une fois..."
    "Once upon a time..."

# game/script.rpy:38
translate english start_3fe379fd:

    # "dans une contrée lointaine..."
    "In a land far, far away..."

# game/script.rpy:49
translate english start_9724a6ca:

    # c "M'maaan !"
    c "Mooooom!"

# game/script.rpy:51
translate english start_ea472893:

    # c "J'ai plus l'âge que tu me racontes des histoires !"
    c "I ain't so little that you can tell me stories anymore!"

# game/script.rpy:48
translate english start_bb1d42cf:

    # "Puisque c'est comme ça, jeune fille, rends-toi utile et va porter ces friandises à ta grand-mère."
    "Is that so, young lady? Then make yourself useful and go bring these sweets to your grandmother."


# game/script.rpy:50
translate english start_f69dd553:

    # "Elle est clouée au lit depuis deux jours."
    "She's been bedridden for two days."

translate english strings:

    # game/script.rpy:53
    old "Obéis"
    new "Obey"

    # game/script.rpy:53
    old "Décline la proposition"
    new "Decline"

# game/script.rpy:65
translate english non_d544b5a0:

    # c "'tain, j'en ai marre..."
    c "Geez, I'm so fed-up..."

# game/script.rpy:66
translate english non_09cd9d21:

    # c "Ça tombe toujours sur moi, tu me m'saoûles !"
    c "It's always me bringing her sweets; you tire me out!"

# game/script.rpy:67
translate english non_8ab0e50d:

    # "Petit chaperon, je ne veux pas entendre ce langage !"
    "Little Hood, I don't want to hear anymore back talk from you!"

# game/script.rpy:68
translate english non_c18c9436:

    # c "J'suis plus \"petit\" et d'abord j'irai si j'ai envie !"
    c "I'm not \"little\" anymore and I'll do as I please!"

# game/script.rpy:69
translate english non_e22ca28b:

    # "Ah oui ?"
    "Oh yeah?"

# game/script.rpy:70
translate english non_32158f90:

    # "Eh bien cet après-midi tu restes dans ta chambre et tu finis tes devoirs, je t'interdis de sortir."
    "Then stay in your room this afternoon, and finish your homework young lady. I forbid you from going out!"

# game/script.rpy:72
translate english non_3f39efc1:

    # "Quant à ta soirée de samedi, on en reparlera avec ton père..."
    "We'll discuss this with your father later Little Hood. And, we’ll think twice about letting you go out this Saturday night..."

# game/script.rpy:73
translate english non_18eb1230:

    # c horrified "Heeeiiin ?!"
    c horrified "Whaaaa?!"

translate english strings:

    # game/script.rpy:72
    old "MAUVAISE FIN - PUNIE"
    new "BAD ENDING - GROUNDED"

# game/script.rpy:78
translate english non_b7938e96:

    # c "C'est pas juste !"
    c "Ain't fair!"

# game/script.rpy:78
translate english oui_252fc3a1:

    # c "Pfff... d'accord."
    c "Gee... okay."

# game/script.rpy:79
translate english oui_a6f693ba:

    # "Prends cette galette de riz bio et ce petit pot de beurre."
    "Bring your grandmother this organic cake and this little pot of butter."

# game/script.rpy:85
translate english oui_a17f2aa9:

    # c "Tu crois pas qu'elle a assez grossi comme ça ?"
    c "Don't you think she's fat enough already?"

# game/script.rpy:81
translate english oui_d32a68e6:

    # c "Elle rentre à peine dans sa chaise."
    c "She's barely fitting inside her chair."

# game/script.rpy:82
translate english oui_9e0b1760:

    # "Je ne veux pas t'entendre dire du mal de ta grand-mère."
    "I don't want to hear you say bad things about your grandmother."

# game/script.rpy:83
translate english oui_e6dc4aba:

    # "Même si c'est vrai."
    "...Even if it's true."

# game/script.rpy:84
translate english oui_a6a79a38:

    # c "Bon ben j'y go, à plus."
    c "Ok then I'll be going, see ya soon."

# game/script.rpy:96
translate english oui_023e0c32:

    # c "Au moins j'ai mon smartphone pour me tenir compagnie pendant le trajet..."
    c "At least I have my smartphone to keep me company during the walk..."

# game/script.rpy:97
translate english oui_ac0445ef:

    # c "{i}RT si t'en as marre de te taper les corvées #mere #relou{/i}"
    c "{i}RT if you're fed-up with chores #lol #kidprobs #dumbparents #FML{/i}"

# game/script.rpy:97
translate english oui_33c17ad2:

    # c "{i}Statut : je me tape tout le boulevard Wilson à pied jusqu'au n°173, pfff !{/i}"
    c "{i}Status: OMG! i have to walk ALL THE WAY DOWN Wilson Blvd till I get to #173, grandma's house, ugh!!!{/i}"

# game/script.rpy:99
translate english oui_2e929e80:

    # l "Hello !"
    l "Hello!"

# game/script.rpy:107
translate english oui_86b46b53:

    # c "Euh... Bonjour ?"
    c "Euh... Hello?"

# game/script.rpy:103
translate english oui_c38ba113:

    # l "À te voir ainsi froncer les sourcils, je parie qu'on t'a encore mise à contribution chez toi."
    l "Seeing you're frowning in such a way, I bet you were forced to be the gopher for your Mom again."

# game/script.rpy:103
translate english oui_7afeb3d0:

    # c "Ben euh... ouais... Enfin non, ma mère m'a fait pitié alors j'y vais mais juste pour cette fois."
    c "Well euh... yeah... well no. My mother got me to pity my grandma again, so I'm going but just this once."

# game/script.rpy:110
translate english oui_2e3f02da:

    # c "Faut pas déconner, hein ?"
    c "There's no way in hell she could force me to go!"

# game/script.rpy:105
translate english oui_7ea2776d:

    # l "Hmmm, les parents ont oublié ce que c'est que d'être jeunes et de vouloir profiter,"
    l "Hmmm, parents forgot what it's like to be young and willing to have fun..."

# game/script.rpy:112
translate english oui_94a01060:

    # l "de vivre vraiment quoi !"
    l "To actually live and all!"

# game/script.rpy:107
translate english oui_72de38a3:

    # c "Ouais..."
    c "Yeah..."

# game/script.rpy:108
translate english oui_466ddc9b:

    # l "Ce qu'il faut c'est avoir les tripes d'imposer ses choix, et de préparer les vieux à prendre son indépendance."
    l "What you need is the guts to stick to your guns, and prepare your folks for your independence."

# game/script.rpy:109
translate english oui_070cecfd:

    # l "Je sûr que tu en es capable. T'es jeune, vive, mignonne..."
    l "I'm sure you can do it. You're young, lively, pretty..."

# game/script.rpy:116
translate english oui_615e0b4b:

    # c "Oh, non, v-tu... c'est exagéré !"
    c "Oh no, you... you're exaggerating!"

# game/script.rpy:117
translate english oui_16d9ba91:

    # l "Mais quand on est de jeunes adultes comme toi et moi, on sait que c'est maintenant que ça se passe, après ce sera trop tard, t'es pas d'accord ?"
    l "Young adults like you and I, we know that the real thing is found right now. When you get old, it'll be too late, don't you agree?"

# game/script.rpy:112
translate english oui_11e65e4c:

    # c "Si, mais..."
    c "Yes, but..."

# game/script.rpy:119
translate english oui_871b983d:

    # l grimace "Voilà, et mes parents ils ont vite dû comprendre que je n'étais plus leur \"petit chéri\", ewww !"
    l grimace "I had to make my parents understand I wasn't their \"little darling\" no more; talk about gross!"

# game/script.rpy:120
translate english oui_f8d5f93a:

    # c "Hihi, c'est exactement ce que j'ai dit à ma mère !"
    c "Hehe, that's exactly what I told my mother!"

# game/script.rpy:116
translate english oui_6a17c631:

    # l "Tu vois je t'ai dit que tu avais ça en toi."
    l "See, I told you you got it in you to change."

# game/script.rpy:123
translate english oui_aace1f2c:

    # l "Je ne veux plus voir ce joli front plissé par les soucis, OK ?"
    l "I don't want to see that lovely forehead wrinkled by worries anymore, ok?"

# game/script.rpy:118
translate english oui_c184a9cb:

    # c "Bah, pour cet après-midi c'est mort, mais ouais la prochaine fois ça sera non, hé."
    c "Well, this afternoon I have to deliver the sweets. Next time, I'm sure I'll say no way, hehe."

# game/script.rpy:125
translate english oui_5a56b47b:

    # l "Du coup tu vas où exactement cet après-midi ?"
    l "Where exactly are you going this afternoon?"

# game/script.rpy:120
translate english oui_2eb12df5:

    # c "Euh... je vais voir de la famille... plus loin."
    c "Euh... I'm going to see family... farther away."

# game/script.rpy:127
translate english oui_ba9ba8b9:

    # l "Et c'est où ça \"plus loin\" ?"
    l "Where's that \"farther away\"?"

# game/script.rpy:122
translate english oui_f9a1d08f:

    # c "Ben c'est... par là-bas mais... euh faut peut-être que j'y aille d'ailleurs..."
    c "Well it's... this way but... I should get going..."

# game/script.rpy:129
translate english oui_594618fd:

    # l "Ne me dis pas que tu es {i}pressée{/i} d'y aller ?"
    l "Don't tell me you're {i}eager{/i} to get there?"

# game/script.rpy:130
translate english oui_aa0a97e9:

    # c "Non bien sûr... tiens, je vais faire un petit crochet pour me prendre une glace !"
    c "Of course not! I'll make a detour and get myself an ice cream!"

# game/script.rpy:131
translate english oui_777dfd9c:

    # l "Héhé... Tiens écoute, il a une soirée vendredi, on y va avec des potes et leurs copines."
    l "Hehehe, well listen, there's gonna be party on friday, and me, a few of my buddies, and their girls are going."

# game/script.rpy:132
translate english oui_4887bc70:

    # l "On a besoin de gens comme toi pour montrer à tous comment s'éclater."
    l "Won't you come too? We need people like you to show'em how to truly have a blast!"

# game/script.rpy:133
translate english oui_330b767e:

    # l "Ajoute-moi sur SocialBook, tu cherches \"Loup3769\", ok?, et tu me ping pour que je file les détails."
    l "Add me on SocialBook. My username's \"Wolf3769\", kay? That way I can send you the details quickly."

# game/script.rpy:134
translate english oui_38fa79b1:

    # c "OK, \"Loup3769\", à plus !"
    c "OK, \"Wolf3769\", see ya!"

# game/script.rpy:129
translate english oui_be7f9017:

    # c "Bye."
    c "Bye."

# game/script.rpy:131
translate english oui_fc24a9db:

    # c "(Bon, et maintenant...)"
    c "(OK, and now...)"

translate english strings:

    # game/script.rpy:134
    old "Ajoute Loup sur SocialBook et traîne en chemin"
    new "Add Wolf on SocialBook, and wander around"

    # game/script.rpy:134
    old "Dépêche-toi d'arriver chez Mamie"
    new "Hurry up, and go at Granny's"

# game/script.rpy:141
translate english peaceful_afternoon_e4750528:

    # c "Bon c'est toujours sympa de se faire draguer, mais j'ai pas que ça à faire moi."
    c "Well it's always nice to get hit on and all, but it's not like I've got nothing else to do."

# game/script.rpy:148
translate english peaceful_afternoon_570e0d86:

    # c "Quant à sa demande d'ami, il a fumé ou quoi ? Y'a pas moyen, on se connaît pas."
    c "Was he high or something? Add him on social book?  No way in hell am I doing that, we don't know each other that well!"

# game/script.rpy:146
translate english peaceful_afternoon_b8ca85f8:

    # c "Hello Mamie"
    c "Heya Granny."

# game/script.rpy:153
translate english peaceful_afternoon_ce490635:

    # m "Bonjour ma petite ! Ça me fait bien plaisir de t'avoir avec moi."
    m "Hello darling! I'm quite pleased to have you with me."

# game/script.rpy:148
translate english peaceful_afternoon_d19f855d:

    # c "..."
    c "..."

# game/script.rpy:155
translate english peaceful_afternoon_f7f6f2b2:

    # c "Moi aussi Mamie !"
    c "So am I Granny, to see ya and all!"

# game/script.rpy:156
translate english peaceful_afternoon_3c283c05:

    # m "Que dirais-tu d'un petit scrabble ? Je parie que je te bats encore cette fois-ci, hihi !"
    m "How about a little scrabble? I bet I'll beat you again this time around!"

# game/script.rpy:157
translate english peaceful_afternoon_2b0da868:

    # c "Dans tes rêves ! Tu vas voir..."
    c "In your dreams! Watch me..."

translate english strings:

    # game/script.rpy:155
    old "FIN TRANQUILLE - JEUX"
    new "QUIET ENDING - GAMES"

# game/script.rpy:162
translate english peaceful_afternoon_b06b05d9:

    # m "Mot compte double !"
    m "Double word score!"

# game/script.rpy:162
translate english dangerous_bf56b18c:

    # c "Je parie qu'il a cru que j'étais plus agée, il a la classe quand même."
    c "I bet he believed I was older, he's classy enough."

# game/script.rpy:169
translate english dangerous_e96e85fb:

    # c "Je vais l'ajouter comme ami, ça va faire criser mes copines !"
    c "I'll add him as a friend to get my friends all fired up!"

# game/script.rpy:167
translate english dangerous_e33cf2de:

    # "Chap Petit souhaite vous inviter à rejoindre son groupe d’amis sur SocialBook."
    "Mo Little wants to be friends on SocialBook."

# game/script.rpy:174
translate english dangerous_f5ce8590:

    # "Statut : {i}En plus je me tape tout le boulevard Wilson à pied jusqu'au n°173, pfff !{/i}"
    "Status: {i}OMG! i have to walk ALL THE WAY DOWN Wilson Blvd till I get to #173, grandma's house, ugh!!!{/i}"

# game/script.rpy:169
translate english dangerous_f768daed:

    # l "Petite sotte..."
    l "Little airhead..."

# game/script.rpy:170
translate english dangerous_e91c23ad:

    # l "Hé bien on va aller y faire un tour."
    l "Let's go have a looksie at her house."

# game/script.rpy:177
translate english dangerous_b5ac26c5:

    # l "Voilà, \"Margaret Petit\", c'est le nom sur la première porte à droite."
    l "Here, \"Margaret Little\", that's the name on the first door on the right."

translate english strings:

    # game/script.rpy:180
    old "Aggresse Mamie"
    new "Assault Granny"

    # game/script.rpy:180
    old "Manœuvre en finesse"
    new "Proceed delicately"

# game/script.rpy:193
translate english aggression_dbb27aad:

    # l "(La porte est carrément déverrouillée, trop facile !)"
    l "(The door is unlocked; this is way too easy!)"

# game/script.rpy:198
translate english aggression_e7f5a0bf:

    # m "Qui est-ce ? Que voulez-vous ? Fichez le camp avant que j'appelle la police !"
    m "Who is it? What do you want? Get out of here before I call the cops!"

# game/script.rpy:199
translate english aggression_8109ca3a:

    # l "La ferme la vieille ! Tu vas faire ce que je dis et..."
    l "Shut up old fart! You'll do as I say and..."

# game/script.rpy:202
translate english aggression_95f35673:

    # l "AAAARRRRRRGHHHH !!!"
    l "AAAARRRRRRGHHHH!!!"

# game/script.rpy:198
translate english aggression_c5e4a5c2:

    # m "Monsieur, voici Suzie, ma fidèle petite bombe lacrymo. Suzie, re-présente toi au monsieur."
    m "Sir, here's Suzie, my faithful little pepper spray. Suzie, introduce yourself again to this fellow."

# game/script.rpy:206
translate english aggression_63f4d5bb:

    # l "UUURRRRRRRRRGGGGGH !!!!"
    l "UUURRRRRRRRRGGGGGH!!!!"

# game/script.rpy:207
translate english aggression_1bf81172:

    # m "Et maintenant, comme promis, vous allez également faire connaissance avec les forces de l'ordre. Non mais !"
    m "And now, as promised, you will meet with the police force as well. Honestly!"

translate english strings:

    # game/script.rpy:211
    old "BONNE FIN (... enfin mauvaise fin pour Loup !) - CASE PRISON"
    new "GOOD ENDING (... well bad ending for Wolf!) - JAIL SPACE"

# game/script.rpy:212
translate english aggression_2edc6d98:

    # "Agent" "Chère Madame, la police vous adresse ses félicitations pour votre sang froid.\nÇa mettra du plomb dans la cervelle de cette petite frappe !"
    "Agent" "Dear madam, I would like to inform you that\nthe police commend your swift thinking, and handling of this situation.\nThis will teach this young thug some common sense for sure!"

# game/script.rpy:218
translate english derrick_bc3b2ca2:

    # l "Toc toc !"
    l "Toc toc!"

# game/script.rpy:219
translate english derrick_2e54086f:

    # m "Oui ? Qui est là ?"
    m "Yes? Who's there?"

# game/script.rpy:214
translate english derrick_e16e707a:

    # l "Chère madame PETIT, je suis représentant, et figurez-vous que j'ai sur moi l'INTÉGRALE,"
    l "Dear Mrs Little, I'm a representative here to tell you some amazing news! I have the one and only, super exclusive, COMPLETE BOX SET!"

# game/script.rpy:221
translate english derrick_5fd87d09:

    # l "je dis bien l'INTÉGRALE de DERRICK, à prix SACRIFIÉ, et tenez-vous bien,"
    l "I mean the COMPLETE BOX SET of MURDER, SHE WROTE, at ROCK-BOTTOM PRICES, and hold still..."

# game/script.rpy:222
translate english derrick_cedce34f:

    # l "j'inclus même la confrontation censurée DERRICK/NAVARRO !"
    l "I'll even include never before seen, censored material from MATLOCK and MAGNUM, P.I!"

# game/script.rpy:223
translate english derrick_70b039d5:

    # m "Oohh... Vous savez parler aux femmes vous, hihi !"
    m "Oohh... You silver-tongued devil you, hehe!"

# game/script.rpy:218
translate english derrick_6ded96a9:

    # m "Tirez la chevillette, et la bobinette cherra."
    m "Pull out the peg and the latch will fall."

# game/script.rpy:225
translate english derrick_f791ad33:

    # l "(Euh... ça veut dire que la porte était carrément déverrouillée ?!)"
    l "(Does that mean the door was unlocked this whole time?!)"

# game/script.rpy:222
translate english derrick_e79816f0:

    # m "Alors montrez-moi ça."
    m "So, show me the box set."

# game/script.rpy:229
translate english derrick_9afc4311:

    # m "Mais que faites-vous ? À l'... MMMHHH !"
    m "Wait, what are you doing? Hel... MMMHHH!"

# game/script.rpy:233
translate english derrick_053e7bbd:

    # c "Toc toc, Mamie, ouh ouh !"
    c "Toc toc, Granny, hey hey!"

# game/script.rpy:234
translate english derrick_ed7c988b:

    # l "Oui ? Qui est là ?"
    l "Yes? Who's there?"

# game/script.rpy:235
translate english derrick_06610742:

    # c "Bah c'est moi, Chap', enfin !"
    c "Well it's me, Mo', who the heck do you think it is?"

# game/script.rpy:230
translate english derrick_78bafe8a:

    # l "Tire la chevillette, et la bobinette cherra."
    l "Oh heavens to Betsy, of course it's my favorite grandchild! Pull out the peg and the latch will fall, darling."

# game/script.rpy:237
translate english derrick_c03c743f:

    # c "Raaah, Mamie ! Essaie de parler un peu moins vieux, quoi, t'abuses."
    c "Arrrh, Granny! Try and talk less old, for chrissake, you're pushing it."

# game/script.rpy:232
translate english derrick_bb8a8750:

    # c "Puis je sais ouvrir une porte quand même."
    c "I know how to open a door, damn it."

# game/script.rpy:242
translate english derrick_359c520f:

    # c "Bah, qu'est-ce que tu t'es mis sur le visage Mamie ?"
    c "What's on your face Granny?"

# game/script.rpy:243
translate english derrick_6045f2f0:

    # c "Tu t'es crue dans une série US ? C'est pas des concombres qui vont t'enlever tes rides, hé !"
    c "Did ya' think you were on TV or something?  Ain't any cucumbers around that'll get those wrinkles out, heh!"

# game/script.rpy:238
translate english derrick_7d5dedbc:

    # l "Hrmm, ma petite, quand tu auras mon âge, on en reparlera."
    l "Hmm, my little girl, when you're my age, you'll think differently."

# game/script.rpy:245
translate english derrick_f479b771:

    # c "Hmmmm..."
    c "Hmmm..."

translate english strings:

    # game/script.rpy:242
    old "Ne contrarie pas Mamie"
    new "Don't upset Granny"

    # game/script.rpy:242
    old "Enfile tes lunettes (même si tu es moche avec)"
    new "Put on your glasses (even if you're ugly with them)"

# game/script.rpy:254
translate english stay_f04d0381:

    # l "Mets-nous un petit peu de ta musique de jeunes."
    l "Let's listen to some of your youngster music."

# game/script.rpy:262
translate english stay_d899703d:

    # l "Plus fort, comme tu as l'habitude, hihi !"
    l "As loud as you want, just like you're used to, hehe!"

# game/script.rpy:256
translate english stay_9ace33e1:

    # l "Maintenant approche mon enfant..."
    l "Now get closer my child..."

# game/script.rpy:264
translate english stay_04266131:

    # c "Oui... ?"
    c "Yes...?"

# game/script.rpy:265
translate english stay_504ad920:

    # c "Hé lâche-moi !"
    c "Hey, get off me!"

# game/script.rpy:266
translate english stay_b29ad987:

    # c "Mais qui êtes-vous ?"
    c "Who are you?"

# game/script.rpy:267
translate english stay_5b667616:

    # c "À l'aide ! Au secours ! Maman !"
    c "Help! Help me! Mommy!"

# game/script.rpy:261
translate english stay_46290bd2:

    # l "Laisse-toi faire..."
    l "Easy now... It's my treat!"

translate english strings:

    # game/script.rpy:265
    old "MAUVAISE FIN - VICTIME"
    new "BAD ENDING - VICTIM"

# game/script.rpy:266
translate english stay_3efef4e2:

    # "Loup fait quelque chose de très désagréable à Chap'.\nQuand pour finir ses pattes se serrent autour de son cou,\nles ténèbres arrivent comme une délivrance."
    "Wolf is doing very unpleasant things to Mo'.\nAt last, Wolf's paws tighten around her neck.\nThe ensuing darkness comes as a relief."

# game/script.rpy:279
translate english glasses_2b1790c4:

    # c "(Mais c'est Loup ! 'tain y se FOUT DU MONDE quoi, ça ne ve pas se passer comme ça !)"
    c "(That's Wolf! What kind of BULLSHIT is this? There's no way I'm falling for his trap, not gonna happen!)"

# game/script.rpy:273
translate english glasses_9826142a:

    # c "Mamie je reviens j'avais dit à mon petit copain je le rappelais, je fais vite."
    c "Granny? I'll be right back. I told my boyfriend I'd call him back. I'll be quick."

# game/script.rpy:274
translate english glasses_0b402e44:

    # l "(Petit copain, petit copain... Quand je me serais occupé de toi, je n'aurai plus rien à envier à celui-là...)"
    l "(Boyfriend...? When I'm done with her, I won't have any reason to envy him...)"

# game/script.rpy:282
translate english glasses_6b1a68c5:

    # c "Oui, maison de retraite \"Parici Lamonai?\" ? C'est pour une urgence, je suis au 173 boulevard Wilson."
    c "Yes, \"Handoverzycash\" nursing home? It's an emergency, I'm at 173 Wilson boulevard."

# game/script.rpy:276
translate english glasses_2870b95f:

    # c "Ça fait un moment qu'on suspecte quelque chose chez ma grand-mère, Mme Petit, mais là c'est confirmé..."
    c "My family and I have suspected that my grandma, Mrs Little, has been out of it for a while but, now it's confirmed..."

# game/script.rpy:284
translate english glasses_a865ec7d:

    # c "Je viens de lui rendre visite et elle s'est carrément mis des fruits et légumes sur la tête !"
    c "I just paid her a visit a little while ago, and she's dancing around with a bowl of fruit on her head!"

# game/script.rpy:279
translate english glasses_f4aa534a:

    # pa "Très chère mademoiselle, nous arrivons immédiatement pour prendre en charge le cli... patient."
    pa "I'm sorry Miss. We'll come immediately to charge of the cli... patient."

# game/script.rpy:286
translate english glasses_2b2c02d6:

    # c "Je savais que je pouvais compter sur vous !"
    c "I knew I could count on you!"

# game/script.rpy:288
translate english glasses_e791c5c3:

    # pa "C'est ici, attrapez-la !"
    pa "There she is, catch her quick!"

# game/script.rpy:289
translate english glasses_7adb23fe:

    # l "Quoi ? Mais euh, je vous interdis lâchez-moi !"
    l "What? I forbid you from getting any closer; get off me!"

# game/script.rpy:285
translate english glasses_e1b5db74:

    # pa "Soyez raisonnable madame."
    pa "Be reasonable ma'am."

# game/script.rpy:292
translate english glasses_6ebd854a:

    # l "(Bon pas le choix, je dois me démasquer) Mais enfin, tout ceci était une plaisanterie, vous voyez bien que je suis un jeune loup !"
    l "(OK no choice, I have to unmask myself.) Come on, all this was a prank. See, I'm just a young wolf underneath!"

# game/script.rpy:287
translate english glasses_bd59fd90:

    # pa "Mademoiselle, vous aviez raison, elle yoyote complètement, on va la tenir sous bonne garde."
    pa "Miss, you were right, she's completely lost her marbles. We'll keep her under close surveillance."

# game/script.rpy:294
translate english glasses_e8fab1be:

    # l "AU SECOURS !!!!"
    l "HELP!!!"

# game/script.rpy:293
translate english glasses_0a7bb80a:

    # pa "Chère demoiselle Chap', malgré ses tentatives d'évasion, votre grand-mère vit maintenant dans un environnement sûr pour elle. Vous avez fait le bon choix avec nous."
    pa "Dear miss Mo', in spite of her escape attempts, your grandmother now lives in a environment that is secure for her. You made the right choice in the end."

translate english strings:

    # game/script.rpy:298
    old "BONNE FIN (pour Chap' ! - mauvaise fin pour Loup !) - MON PRISONNIER"
    new "GOOD ENDING (for Mo'! - bad ending for Wolf!) - MY PRISONER"

